/* Generated by powerful Codeforces Tool
 * Author: xxxodarap
 * Time: 2023-09-04 10:53
**/

#include<bits/stdc++.h>
using namespace std;
namespace R = std::ranges;

#define F(i, a, e) for(ll i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;


template<class T> bool ckmin(T& a, const T& b) { return b < a ? a = b, 1 : 0; }
template<class T> bool ckmax(T& a, const T& b) { return a < b ? a = b, 1 : 0; }


const lf PI = acos(-1), EPSI = 1e-9;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


// =============== Solution section ===============
const ll MAXN = 1e6 + 5;
const ll INF = 1LL << 60;

// from reference
vector<int> edges[MAXN];
int tin[MAXN], d[MAXN], t = 0;
int st[__bit_width(MAXN) + 1][2 * MAXN];
void dfs(int v, int de = 1){
  d[v] = de, st[0][tin[v] = ++t] = v;
  for(int w : edges[v])
    if(!tin[w]) dfs(w, de + 1), st[0][++t] = v;
}
void build(int root, int n){
  dfs(root);
  for(int k = 0; k < __bit_width(n); k++)
    for(int i = 0; i + (2 << k) <= 2 * n; i++){
      int t1 = st[k][i], t2 = st[k][i + (1 << k)];
      st[k + 1][i] = d[t1] < d[t2] ? t1 : t2;
    }
}
int lca(int a, int b){
  auto [l, r] = minmax(tin[a], tin[b]);
  int k = __bit_width(r - l + 1) - 1;
  int t1 = st[k][l], t2 = st[k][r - (1 << k) + 1];
  return d[t1] < d[t2] ? t1 : t2;
}
int dist(int a, int b){
  return d[a] + d[b] - 2 * d[lca(a, b)];
}
// from reference 

ll H, W;

void addedge(int x, int y){
    edges[x].push_back(y);
    edges[y].push_back(x);
}

void solve(){
    // Code goes here ...
    cin >> H >> W;

    vector<vector<int>> grid(H, vector<int>(W, 0));

    int vid = 0;
    string row, lastrow;
    getline(cin, row);
    getline(cin, row);
    R(i, H){
        lastrow = row;
        getline(cin, row);
        R(j, W){
            int jpos = 2 * j + 1;
            grid[i][j] = vid;

            if(i > 0 and lastrow[jpos] != '_')
                addedge(vid, vid - W);
            if(j > 0 and row[jpos - 1] != '|')
                addedge(vid, vid - 1);

            vid++;
        }
    }


    int M;
    cin >> M;

    vector<int> xs(M), ys(M);
    R(i, M) cin >> xs[i] >> ys[i];

    vector<pii> qs(M-1);
    R(i, M - 1)
        qs[i] = {
            grid[xs[i] - 1][ys[i] - 1],
            grid[xs[i + 1] - 1][ys[i + 1] - 1]
        };


    // usage
    build(0, vid);
    ll res = 0;
    for(auto [a, b] : qs)
        res += dist(a, b);
    // usage

    cout << res << endl;
}


// =============== Main section ===============
int main(){
    ios_base::sync_with_stdio(0); cin.tie(0);
    cout << setprecision(12); // << fixed;

    solve();

    return 0;
}
        
