#include<bits/stdc++.h>
using namespace std;

#define F(i, a, e) for(int i = (a); i < (e); i++)

typedef long long ll;

const int MAXN = 2e5 + 10;
vector<int> edges[MAXN], inedges[MAXN], topsort, used;

vector<ll> costs;

void dfs_topsort(int v){
    used[v] = 1;
    for(auto u: edges[v])
        if(!used[u])
            dfs_topsort(u);
    topsort.push_back(v);
}
void find_topsort (int n){
    for(int i = 0; i < n; i++)
        if(!used[i])
            dfs_topsort(i);
    reverse(topsort.begin(), topsort.end());
}


void solve(){
    int N, K;
    cin >> N >> K;

    ll c;
    F(i, 0, N){
        cin >> c;
        costs.push_back(c);
    }

    int id;
    F(i, 0, K){
        cin >> id;
        costs[id-1] = 0;
    }

    int num, inid;
    F(out, 0, N){
        cin >> num;
        F(j, 0, num){
            cin >> inid;
            edges[inid-1].push_back(out);
            inedges[out].push_back(inid-1);
        }
    }
        
    used = vector<int>(N, 0);
    find_topsort(N);

    ll brewcost;
    for(int node: topsort){
        if(inedges[node].empty()) continue;
        brewcost = 0;
        for(int innode: inedges[node])
            brewcost += costs[innode];
        costs[node] = min(costs[node], brewcost);
    }

    /* ll res = accumulate(costs, costs+N,0); */
    for(ll c: costs){
        cout << c << " ";
    }
    cout << endl;


    // cleanup
    F(i, 0, N){
        edges[i].clear();
        inedges[i].clear();
    }
    topsort.clear();
    costs.clear();
    used.clear();
}


int main(){
    int T;
    cin >> T;
    while(T--)
        solve();
}
        
