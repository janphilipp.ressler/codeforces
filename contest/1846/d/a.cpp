#include<bits/stdc++.h>
using namespace std;

#define F(i, a, e) for(int i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef long double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;

const int MAXN = 2e5 + 5;
const int INF = 1 << 30;

lf base, height;
lf area (ll start, ll end){
    assert ( end > start );
    lf fraction = min(1.0l, (end - start) / height);
    return ((2-fraction) * base) / 2 * fraction * height;
}

void solve(){
    int N;
    lf res = 0.0;
    cin >> N >> base >> height;
    
    // Code goes here ...
    ll last, cur;
    cin >> last;
    while(--N){
        cin >> cur;
        res += area(last, cur);
        last = cur;
    }
    res += area(last, INF);

    cout << res << endl;
}

int main(){
    cout << fixed << setprecision(8);
    int T;
    cin >> T;
    while(T--)
        solve();
}
        
