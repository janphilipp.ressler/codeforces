/* Generated by powerful Codeforces Tool
 * Author: xxxodarap
 * Time: 2023-08-31 16:53
**/

#include<bits/stdc++.h>
using namespace std;
namespace R = std::ranges;

#define F(i, a, e) for(ll i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;


template<class T> bool ckmin(T& a, const T& b) { return b < a ? a = b, 1 : 0; }
template<class T> bool ckmax(T& a, const T& b) { return a < b ? a = b, 1 : 0; }


const lf PI = acos(-1), EPSI = 1e-9;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


// =============== Solution section ===============
const int MAXN = 2e5 + 5;
const int INF = 1e7;

string seq;

void solve(){
    // Code goes here ...
    cin >> seq;

    int num = 0;
    int notsortedfrom = INF;
    int sortedatleastto = 1;
    R(i, ssize(seq)){
        switch(seq[i]){
            case '+':
                num++;
                break;
            case '-':
                if(num <= 0){
                    cout << "NO" << endl;
                    return;
                }
                num--;
                if(num < 2 or num < notsortedfrom)
                    notsortedfrom = INF;
                ckmin(sortedatleastto, num);
                ckmax(sortedatleastto, 1);

                break;
            case '1':
                // is false if notsortedatpos <= num
                if(notsortedfrom <= num){
                    cout << "NO" << endl;
                    return;
                }
                // if true; sortedatleastto update
                ckmax(sortedatleastto, num);
                notsortedfrom = INF;

                break;
            case '0':
                // is false if sortedatleastto >= num
                if(sortedatleastto >= num){
                    cout << "NO" << endl;
                    return;
                }
                ckmin(notsortedfrom, num);

                break;
            default:
                cout << "Big Problem" << endl;
        }
    }

    cout << "YES" << endl;
}


// =============== Main section ===============
int main(){
    ios_base::sync_with_stdio(0); cin.tie(0);
    cout << setprecision(12); // << fixed;

    int T = 1;
    cin >> T;
    while(T--)
        solve();

    return 0;
}
        
