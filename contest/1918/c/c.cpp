/* Generated by powerful Codeforces Tool
 * Author: $%U%$
 * Time: $%Y%$-$%M%$-$%D%$ $%h%$:$%m%$
 **/

#include<bits/stdc++.h>
using namespace std;
namespace R = std::ranges;

#define F(i, a, e) for(ll i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef unsigned long long ull;
typedef double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;


template<class T> bool ckmin(T& a, const T& b){ return b < a ? a = b, 1 : 0; }
template<class T> bool ckmax(T& a, const T& b){ return a < b ? a = b, 1 : 0; }


const lf PI = acos(-1), EPSI = 1e-9;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


// =============== Solution section ===============
const ll MOD = 998'244'353;

const ll MAXN = 2e5 + 5;
const ll INF = 1LL << 60;

ll N, M;

void solve(){
    // Code goes here ...
    ull a, b, r;
    cin >> a >> b >> r;

    if(a < b) swap(a, b);
    if(a == b){
        cout << 0 << endl;
        return;
    }

    int aa = bit_width(a);
    int bb = bit_width(b);
    int rr = bit_width(r);

    // we know aa >= bb
    while(aa == bb){
        a -= 1ull << (aa - 1);
        b -= 1ull << (bb - 1);
        aa = bit_width(a);
        bb = bit_width(b);
        // cerr << aa << " v " << b << endl;
    }

    // now either aa == 0 or aa > bb
    if(aa == 0){
        cout << 0 << endl;
        return;
    }

    // now try to reduce the rest out of it by making a xor x as small as possible
    bitset<62> as(a), bs(b), rs(r);

    // cerr << as << " " << aa << endl;
    // cerr << bs << " " << bb << endl;

    bool r_ava = false;
    if(rr >= aa){
        r_ava = true;
        rr = aa - 1;
    }
    // cerr << rr << endl;
    for(int i = max(rr - 1, -1); i >= 0; i--){
        if(as[i] and not bs[i]){
            if(r_ava or rs[i]){
                bs[i] = bs[i] ^ as[i];
                as[i] = as[i] ^ as[i];
            }
        }
        else if(rs[i]){
            r_ava = true;
        }
    }

    // cerr << as << endl;
    // cerr << bs << endl;

    ull res = as.to_ullong() - bs.to_ullong();
    cout << res << endl;
}


// =============== Main section ===============
int main(){
    ios_base::sync_with_stdio(0); cin.tie(0);
    cout << setprecision(12); // << fixed;

    int T = 1;
    cin >> T;
    while(T--)
        solve();

    /* string rest; */
    /* while(cin.peek() != char_traits<char>::eof()){ */
    /*     solve(); */
    /*     getline(cin, rest); */
    /* } */

    return 0;
}
        
