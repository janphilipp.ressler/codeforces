from collections import defaultdict

letters = [chr(ord('a') + i) for i in range(26)]
# letters.reverse()

def solve():
  n = int(input())
  a = [int(x) for x in input().split()]

  d = defaultdict(list)
  d[0] = letters.copy()
  
  # s = []
  s = ''
  for x in a:
    c = d[x].pop()
    d[x + 1].append(c)

    # s.append(c)
    s += c

    # lololol
    # yo = s
    # yo += 'x'

  # print(''.join(s))
  print(s)

t = int(input())

for _ in range(t):
  solve()
