#include<bits/stdc++.h>
using namespace std;

#define F(i, a, e) for(int i = (a); i < (e); i++)

typedef long long ll;
typedef long double lf;
typedef pair<int, int> pii;

const int MAXN = 1e5 + 10;

void solve(){
    int N;
    cin >> N;

    vector<pii> a;
    int amounts[N+1], prefix[N+1], res[N];
    fill(amounts, amounts + N + 2, 0);
    int x;
    F(i, 0, N){
        cin >> x;
        a.push_back({x, i});
        amounts[x]++;
    }

    prefix[N] = amounts[N];
    for(int i = N-1; i >= 0; i--)
        prefix[i] = amounts[i] + prefix[i + 1];

    F(i, 0, N+1){
        if(prefix[prefix[i]] < i){
            cout << "NO" << endl;
            return;
        }
    }
    cout << "YES" << endl;

    sort(a.begin(), a.end());

    int curvalue = 0, curnum = N;
    bool curtoggle = a[0].first == 0;
    int read = 0, pre = 0, post = N-1;
    while(pre <= post){
        if(a[read].first == curvalue){
            /* fprintf(stderr, "%d gets %d\n", a[pre].second, curnum); */
            res[a[pre++].second] = curnum * (curtoggle ? -1 : 1);
            read++;
        }
        else{
            if(read != 0){
                curnum--;
                curtoggle = !curtoggle;
            }
            F(j, 0, a[read].first - curvalue){
                /* fprintf(stderr, "%d gets %d\n", a[post].second, curnum); */
                res[a[post--].second] = curnum * (curtoggle ? -1 : 1);
                if(pre > post) break;
            }
            curnum--;
            curtoggle = !curtoggle;
            curvalue = a[read].first;
        }
    }

    F(i, 0, N)
        cout << res[i] << " ";
    cout << endl;

}

int main(){
    int T;
    cin >> T;
    while(T--)
        solve();
}
        
