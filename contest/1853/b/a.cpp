#include<bits/stdc++.h>
using namespace std;

#define F(i, a, e) for(int i = (a); i < (e); i++)

typedef long long ll;
typedef long double lf;

const int MAXN = 2e5 + 10;

const vector<int>fibs = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418};


void solve(){
    int N, K;
    cin >> N >> K;

    if(K >= 29){
        cout << 0 << endl;
        return;
    }

    int counter = 0;
    int Fa = fibs[K-2], Fb = fibs[K-1];
    int a, b = floor((lf) N / Fb);

    while(1){
        a = (N - b * Fb) / Fa;
        if( a > b) break;
        if(Fa * a + Fb * b == N)
            counter++;
        b--;
    }
    cout << counter << endl;
}

int main(){
    int T;
    cin >> T;
    while(T--)
        solve();
}
        
