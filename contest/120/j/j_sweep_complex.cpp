/* Generated by powerful Codeforces Tool
 * Author: xxxodarap
 * Time: 2023-09-08 01:16
**/

#include<bits/stdc++.h>
using namespace std;
namespace R = std::ranges;

#define F(i, a, e) for(ll i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef double lf;
/* typedef complex<lf> cp; */
typedef pair<int, int> pii;


template<class T> bool ckmin(T& a, const T& b) { return b < a ? a = b, 1 : 0; }
template<class T> bool ckmax(T& a, const T& b) { return a < b ? a = b, 1 : 0; }


const lf PI = acos(-1), EPSI = 1e-9;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


// =============== Solution section ===============
const ll MAXN = 2e5 + 5;
const ll INF = 1LL << 60;

// from reference
struct cp : complex<lf> {
  using complex::complex;   // inheriting ctors
  int id, k;                   // extensions ...
};
cp A, B;
bool cm(cp a, cp b){ return imag(a) < imag(b); }
lf closest(vector<cp> P){
  R::sort(P, {}, [](cp a){ return real(a); });
  set<cp, decltype(&cm)> s(&cm);
  lf mn = INFINITY;
  auto ptr = begin(P);
  for(cp p : P){
    while(ptr != end(P) &&
          real(p) - real(*ptr) >= mn)
      s.erase(*ptr++);
    auto it1 = s.lower_bound((cp)(p - mn * 1i));
    auto it2 = s.upper_bound((cp)(p + mn * 1i));
    for(cp k : R::subrange(it1, it2))
      if(lf d = abs(p - k); d < mn)
        mn = d, A = p, B = k;
    s.insert(p);
  }
  return mn;
}
// ===

ifstream fin("input.txt");
ofstream fout("output.txt");

using pt = cp;

ll N;

void solve(){
    // Code goes here ...
    fin >> N;
    vector<int> xs(N), ys(N);
    R(i, N) fin >> xs[i] >> ys[i];  
    
    vector<pt> points(N);
    R(i, N){
        int k = 1 + (xs[i] < 0 ? 1 : 0) + (ys[i] < 0 ? 2 : 0);
        points[i] = {abs(xs[i]), abs(ys[i])};
        points[i].id = i;
        points[i].k = k;
    }

    closest(points);

    int ia = A.id, ka = A.k;
    int ib = B.id, kb = B.k;

    cerr << "Closest points are: " << A << " and " << B << endl;
    cerr << " with conversion numbers " << ka << " and " << 5 - kb << endl;

    fout << ia + 1 << " " << ka << " " << ib + 1 << " " << 5 - kb << endl;
}


// =============== Main section ===============
int main(){
    solve();
    return 0;
}
        
