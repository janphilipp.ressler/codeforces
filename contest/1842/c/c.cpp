/* Generated by powerful Codeforces Tool
 * Author: xxxodarap
 * Time: 2023-07-28 20:02
**/

#include<bits/stdc++.h>
using namespace std;

#define F(i, a, e) for(int i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef long double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;


void solve(){
    int N;
    cin >> N;
    
    // Code goes here ...
    int balls[N];
    R(i, N)
        cin >> balls[i];

    vector<vector<int>> bestupto(N, vector<int>{0,0}); // [N][2] {0};
    vector<int> last(N + 1, -1);
    bestupto[0][0] = 0;
    bestupto[0][1] = 0;
    last[balls[0]] = 0;

    F(i, 1, N){
        bestupto[i][0] = max(bestupto[i - 1][0], bestupto[i - 1][1]);
        int lastuse = last[balls[i]];
        if(lastuse != -1)
            bestupto[i][1] = max(bestupto[lastuse][1] + (i - lastuse), bestupto[lastuse][0] + (i - lastuse + 1));
        else
            bestupto[i][1] = bestupto[i][0];
        last[balls[i]] = i;
    }

    cout << max(bestupto[N-1][0], bestupto[N-1][1]) << endl;
}

int main(){
    cout << fixed << setprecision(8);
    int T;
    cin >> T;
    while(T--)
        solve();
}
        
