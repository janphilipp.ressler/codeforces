/* Generated by powerful Codeforces Tool
 * Author: xxxodarap
 * Time: 2023-08-08 00:28
**/

#include<bits/stdc++.h>
using namespace std;

#define F(i, a, e) for(ll i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef long double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;

ll N;
const lf PI = acos(-1);


lf angle(const cp &a){
    return fmod(arg(a) + 2 * PI, 2*PI);
}
bool cmp_ang(const cp &l, const cp &r) {
    if(l.imag() == 0 and l.real() >= 0)
        return 1;
    return angle(l) < angle(r);
}

void show(const cp &a){
    cerr << a << " has " << angle(a) << endl;
}

lf cross ( cp a , cp b) { return ( conj (a) * b) . imag () ; }
bool cmp_ang2(const cp &l, const cp &r) {
    bool x = imag(l) >= 0, y = imag(r) >= 0;
    return x == y ? cross(l, r) > 0 : imag(l) >= 0;
/*     auto pl = make_pair(l.real(), l.imag()), pr = make_pair(r.real(), r.imag()), z = make_pair(0.0L,0.0L); */
/*     bool x = pl < z, y = pr < z; */
/*     return x == y ? cross(l, r) > 0 : pl < pr; */
}

random_device rd;
mt19937 gen(rd());

int main(){
    ios_base::sync_with_stdio(false);
    cout << fixed << setprecision(8);
    cerr << setprecision(22);

    /* cp x; */
    /* for(int i = -3; i < 9; i++){ */
    /*     x = cp(100, -i); */
    /*     show(x); */
    /*     /1* cerr << angle({1, i}) << endl; *1/ */
    /* } */

    /* show(cp(1, 0.0)); */
    /* show(cp(1, -0.0)); */
    /* show(cp(-1, 0.0)); */
    /* show(cp(-1, -0.0)); */

    /* show(cp(1e16, 1)); */
    /* show(cp(1e17, 1)); */
    /* show(cp(1e18, 1)); */
    /* show(cp(1e19, 1)); */
    /* show(cp(1e16, -1)); */
    /* show(cp(1e17, -1)); */
    /* show(cp(1e18, -1)); */
    /* show(cp(1e19, -1)); */


    // compare part
    vector<cp> v = {{1,0}, {0,1}, {-1, 0}, {0, -1}, {1, 1}, {-1, -1}, {-1, 1}, {1, -1}};
    shuffle(begin(v), end(v), gen);


    cerr << "Now sorting" << endl;
    sort(begin(v), end(v), cmp_ang2);
    for(auto ele: v)
        cerr << ele << endl;

    auto it = lower_bound(begin(v), end(v), cp(1, 0), cmp_ang2);
    cerr << "Element" << *it << endl;
    rotate(begin(v), it, end(v));

    for(auto ele: v)
        cerr << ele << endl;
}
        
