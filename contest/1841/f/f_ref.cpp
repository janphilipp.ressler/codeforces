#include<bits/stdc++.h>
using namespace std;

// from Reference
typedef double lf;
typedef complex<lf> cp;
typedef vector<cp> poly;
const double PI = acos(-1), EPSI = 1e-9;
// alread exist: lf abs(cp), cp polar(lf, lf), lf arg(cp), constants 0 + 1i
lf dot(cp a, cp b) { return (conj(a) * b).real(); }
lf cross(cp a, cp b) { return (conj(a) * b).imag(); }

bool cmp_bottomleft(const cp &l, const cp &r) {
  return pair(imag(l), real(l)) < pair(imag(r), real(r));
}

vector<cp> minkowski2(vector<cp> P, vector<cp> Q){
  rotate(begin(P), min_element(begin(P), end(P), cmp_bottomleft), end(P));
  rotate(begin(Q), min_element(begin(Q), end(Q), cmp_bottomleft), end(Q));
  vector<cp> res; res.reserve(size(P) + size(Q));
  P.insert(end(P), {P[0], P[1]});
  Q.insert(end(Q), {Q[0], Q[1]});
  size_t i = 0, j = 0;
  while(i < size(P) - 2 || j < size(Q) - 2){
    res.emplace_back(P[i] + Q[j]);
    lf c = cross(P[i + 1] - P[i], Q[j + 1] - Q[j]);
    if(c >= 0 && i < size(P) - 2) ++i;
    if(c <= 0 && j < size(Q) - 2) ++j;
  }
  return res;
}


// main part
typedef long long ll;
ll N;
queue<poly> q;
void solve(){
    cin >> N;
    ll a, b, c, d, x, y;
    cp z, zero = {0,0};
    for (int i = 0; i < N; ++i) {
        cin >> a >> b >> c >> d;
        z = {a - b, c - d};
        if(z == zero) continue;
        q.push({z, zero});
	}

    while(size(q) >= 2){
        auto a = q.front(); q.pop();
        auto b = q.front(); q.pop();
        q.push(minkowski2(a,b));
    }

    lf ans = 0;
    if(empty(q)){
        cout << ans << endl;
        return;
    }
    for (auto e: q.front())
        ans = max(ans, norm(e));
    cout << ans << endl;
}

int main(){
    ios_base::sync_with_stdio(0);
    cout << setprecision(12);
    cin.tie(0);

    solve();
}
