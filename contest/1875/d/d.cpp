/* Generated by powerful Codeforces Tool
 * Author: xxxodarap
 * Time: 2023-09-30 18:07
 **/

#include<bits/stdc++.h>
using namespace std;
namespace R = std::ranges;

#define F(i, a, e) for(ll i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;


template<class T> bool ckmin(T& a, const T& b){ return b < a ? a = b, 1 : 0; }
template<class T> bool ckmax(T& a, const T& b){ return a < b ? a = b, 1 : 0; }


const lf PI = acos(-1), EPSI = 1e-9;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


// =============== Solution section ===============
const ll MOD = 998'244'353;

const ll MAXN = 2e5 + 5;
const ll INF = 1LL << 60;

ll N;

void solve(){
    // Code goes here ...
    cin >> N;
    vector<ll> a(N);
    R(i, N) cin >> a[i];

    multiset<ll> aset(begin(a), end(a));
    ll zerocnt = aset.count(0);
    if(zerocnt == 0){
        cout << 0 << endl;
        return;
    }

    ll MEX;
    R(i, 5004)
        if(not aset.contains(i)){
            MEX = i;
            break;
        }

    vector<vector<ll>> dp(MEX + 3, vector<ll>(MEX + 3));
    R(i, MEX + 1){
        dp[0][i] = (zerocnt - 1) * i;
    }
    F(i, 1, MEX){
        ll icount = aset.count(i);
        F(j, 1, MEX + 1){
            ll mex = j;
            dp[i][mex] = min(
                    (icount - 1) * mex + i + dp[i - 1][i],
                    dp[i - 1][mex]
                    );
        }
    }

    ll res = INF;
    R(i, MEX){
        // cerr << dp[i][MEX] << " ";
        ckmin(res, dp[i][MEX]);
    }
    // cerr << endl;
    cout << res << endl;
}


// =============== Main section ===============
int main(){
    ios_base::sync_with_stdio(0); cin.tie(0);
    cout << setprecision(12); // << fixed;

    int T = 1;
    cin >> T;
    while(T--)
        solve();

    /* string rest; */
    /* while(cin.peek() != char_traits<char>::eof()){ */
    /*     solve(); */
    /*     getline(cin, rest); */
    /* } */

    return 0;
}
        
