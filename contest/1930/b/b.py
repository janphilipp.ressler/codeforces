
def solve():
  n = int(input())
  
  toggle = True
  ls = []
  i = 0
  for _ in range(n):
    if toggle:
      ls.append(n - i)
    else:
      i += 1
      ls.append(i)

    toggle = not toggle

  print(*ls)

t = int(input())

for _ in range(t):
  solve()

