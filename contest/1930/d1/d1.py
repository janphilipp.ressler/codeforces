from functools import lru_cache
@lru_cache(maxsize = None)
def f(s: str):
  ls = ["0"]
  for c in s:
    if c == "1" and "1" not in ls[-2:]:
      ls.append("1")
    else:
      ls.append("0")
  # print(s, "".join(ls))
  return ls.count("1")



def solve():
  n = int(input())
  s = input()

  r = 0
  for i in range(n):
    for j in range(i, n):
      r += f(s[i:j+1])
  print(r)


t = int(input())
for _ in range(t):
  solve()

