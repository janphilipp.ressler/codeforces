
def solve():
  n = int(input())
  ls = list(int(x) for x in input().split())
  ls.sort()

  r = sum(ls[::2])
  print(r)


t = int(input())

for _ in range(t):
  solve()
