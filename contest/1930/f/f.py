
def solve():
  n, q = map(int, input().split())
  es = list(int(x) for x in input().split())

  bintree = dict()

  # init
  ls = [0]
  a = []
  mi, ma = 2 ** 22, 0

  def insert(val: str, tree: dict):
    if val == "":
      return

    k = val[0]
    val = val[1:]

    if k not in tree.keys():
      tree[k] = dict()

    insert(val, tree[k])

  def findmax(orval, tree: dict):
    if orval == "":
      return ""
    
    # if len(orval) < 8:
    #   print("searching", orval, "in", tree)

    k = orval[0]
    orval = orval[1:]

    ls = []
    for kk in tree.keys():
      r = findmax(orval, tree[kk])
      r = max(kk, k) + r
      ls.append(r)
    return max(ls)

  def binstr(x: int):
    s = bin(x)[2:]
    s = s.zfill(22)
    return s

  for e in es:
    v = e + ls[-1]
    v %= n
    a.append(v)
    insert(binstr(v), bintree)

    mi = min(mi, v)
    # print("mi", mi, binstr(mi))
    # find ma
    ma = findmax(binstr(mi), bintree)
    ma = int(ma, 2)
    # print(ma, mi)

    # ls.append((ma | mi) - mi)
    ls.append(ma - mi)

  print(*ls[1:])
  # print(*a)


t = int(input())
for _ in range(t):
  solve()
