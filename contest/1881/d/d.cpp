/* Generated by powerful Codeforces Tool
 * Author: xxxodarap
 * Time: 2023-10-12 17:17
 **/

#include<bits/stdc++.h>
using namespace std;
namespace R = std::ranges;

#define F(i, a, e) for(ll i = (a); i < (e); i++)
#define R(i, e) F(i, 0, e)

typedef long long ll;
typedef double lf;
typedef complex<lf> cp;
typedef pair<int, int> pii;


template<class T> bool ckmin(T& a, const T& b){ return b < a ? a = b, 1 : 0; }
template<class T> bool ckmax(T& a, const T& b){ return a < b ? a = b, 1 : 0; }


const lf PI = acos(-1), EPSI = 1e-9;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


// =============== Solution section ===============
const ll MOD = 998'244'353;

const ll MAXN = 2e5 + 5;
const ll INF = 1LL << 60;

ll N;

vector<pair<ll, int>> factorization(ll n){
  vector<pair<ll, int>> factors;
  for(ll cnt, i = 2; i * i <= n; i++){
    for(cnt = 0; n % i == 0; n /= i, cnt++);
    if(cnt) factors.emplace_back(i, cnt);
  }
  if(n > 1) factors.emplace_back(n, 1);
  return factors;
}


void solve(){
    // Code goes here ...
    cin >> N;
    map<ll, ll> counts;

    vector<ll> a(N);
    R(i, N) cin >> a[i];

    for(auto ele : a){
        auto v = factorization(ele);
        for(auto [p, k] : v){
            counts[p] += k;
        }
    }

    bool res = 1;
    for(auto [p, k] : counts){
        // fprintf(stderr, "%lld times %lld\n", p, k);
        res = res and k % N == 0;
    }
    
    cout << (res ? "YES" : "NO") << endl;
}


// =============== Main section ===============
int main(){
    ios_base::sync_with_stdio(0); cin.tie(0);
    cout << setprecision(12); // << fixed;

    int T = 1;
    cin >> T;
    while(T--)
        solve();

    /* string rest; */
    /* while(cin.peek() != char_traits<char>::eof()){ */
    /*     solve(); */
    /*     getline(cin, rest); */
    /* } */

    return 0;
}
        
