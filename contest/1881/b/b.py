t = int(input())

for _ in range(t):
  a, b, c = map(int, input().split())

  s = sum([a, b, c])
  res = False

  if s % 3 == 0 and a == b == c:
    res = True

  for amount in [3, 4, 5, 6]:
    if s % amount == 0:
      d = int(s / amount)
      if a % d == 0 and b % d == 0 and c % d == 0:
        res = True

  print("YES" if res else "NO")





