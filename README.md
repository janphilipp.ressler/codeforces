


### cf

cf is a command line tool to fetch input/output samples and submit code to codeforces.com


https://github.com/xalanq/cf-tool

See here for sample usage: https://github.com/xalanq/cf-tool#usage

template.cpp is a template which is used by `cf gen`.


Other useful commands are `cf test`, `cf submit`, `cf parse` and of course `cf race`.




