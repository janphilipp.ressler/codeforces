#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin >> t;
    int n;
    for(int i=0; i<t; i++){
        cin >> n;

        int a[n];
        /* vector<int> pos, neg; */
        for(int j=0; j<n; j++){
            cin >> a[j];
            /* if(a[j] == 1) */
            /*     pos.push_back(j); */
            /* else if(a[j] == -1) */
            /*     neg.push_back(j); */
            /* else */
            /*     cerr << j << " neither 1 or -1 " << a[j] << endl; */
        }

        if(n%2 == 1){
            cout << -1 << endl;
            continue;
        }

        set<int> starts, ends, divs;
        /* starts.insert(0); */
        /* ends.insert(n); */
        int uno, due;
        for(int j=0; j<n; j+=2){
            uno = a[j];
            due = a[j+1];
            starts.insert(j);
            ends.insert(j+1);
            if(uno * due == -1){
                ends.insert(j);
                starts.insert(j+1);
            }
        }
        int k = starts.size();
        cout << k << endl;
        auto s = starts.begin(), e = ends.begin();
        /* s = starts.begin(); */
        /* e = ends.begin(); */
        for(int j=0; j<k; j++){
            cout << *s + 1 << " " << *e + 1 << endl;
            s++;
            e++;
        }
        /* int sum = accumulate(a, a+n, 0); */
        /* cerr << "Sum: " << sum << endl; */

    }
}


/* 1 1   = + - */
/* 1 -1  = + + */
/* -1 1  = + + */
/* -1 -1 = + - */

