_, max = map(int, input().split())

input = sorted(map(int, input().split()))

solved = True
save = 0
i = 0
fac = 1

while(fac <= max):
    number_of_facs = 0
    while(i<len(input) and input[i]==fac):
       number_of_facs += 1
       i += 1

    if(save % fac == 0):
       save = (number_of_facs*fac+save) / (fac) 
    else:
       solved = False
       break

    fac += 1
    

print("yes" if solved else "no")
